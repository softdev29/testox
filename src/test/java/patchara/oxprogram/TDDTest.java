package patchara.oxprogram;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TDDTest {
    
    public TDDTest() {
    }
    // input (a,b) result c
    @Test
    public void testAdd_1_2is3(){
        assertEquals(3,Example.add(1,2));
    }
    
    @Test
    public void testAdd_3_2is7(){
        assertEquals(7,Example.add(3,4));
    }
    
    @Test
    public void testAdd_20_22is42(){
        assertEquals(42,Example.add(20,22));
    }
    
    // p:paper, s:scissors, h:hammer
    //choop (char player1, char player2) => "p1", "p2", "draw"
    
    // draw
    @Test
    public void testChoop_p1_p_p2_p_is_draw(){
        assertEquals("draw", Example.choop('p', 'p'));
    }
    
    @Test
    public void testChoop_p1_h_p2_h_is_draw(){
        assertEquals("draw", Example.choop('h', 'h'));
    }
    
    @Test
    public void testChoop_p1_s_p2_s_is_draw(){
        assertEquals("draw", Example.choop('s', 's'));
    }
    
    //p1 win
    
    // s:p
    @Test
    public void testChoop_p1_s_p2_p_is_p1(){
        assertEquals("p1", Example.choop('s', 'p'));
    }
    
    //h:s
    @Test
    public void testChoop_p1_h_p2_s_is_p1(){
        assertEquals("p1", Example.choop('h', 's'));
    }
    
    //p:h
    @Test
    public void testChoop_p1_p_p2_h_is_p1(){
        assertEquals("p1", Example.choop('p', 'h'));
    }
    
    //p2 win
    
    // p:s
    @Test
    public void testChoop_p1_p_p2_s_is_p2(){
        assertEquals("p2", Example.choop('p', 's'));
    }
    
    //s:h
    @Test
    public void testChoop_p1_s_p2_h_is_p2(){
        assertEquals("p2", Example.choop('s', 'h'));
    }
    
    //h:p
    @Test
    public void testChoop_p1_h_p2_p_is_p2(){
        assertEquals("p2", Example.choop('h', 'p'));
    }

}
