
package patchara.oxprogram;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestOX {
    
    public TestOX() {
    }
    
    //draw
    @Test
    public void test_draw() {
        assertEquals(true, OXProgram.checkDraw(8));
    }
    
    // player O
    // vertical
    
    @Test
    public void test_o_win_vertical1() {
        char table[][] = {{'O', '-', '-'}, 
                          {'O', '-', '-'},
                          {'O', '-', '-'}};
        
        assertEquals(true, OXProgram.checkVertical(table,'O',1));
    }
    
    @Test
    public void test_o_win_vertical2() {
        char table[][] = {{'-', 'O', '-'}, 
                          {'-', 'O', '-'},
                          {'-', 'O', '-'}};
        
        assertEquals(true, OXProgram.checkVertical(table,'O',2));
    }
    
    @Test
    public void test_o_win_vertical3() {
        char table[][] = {{'-', '-', 'O'}, 
                          {'-', '-', 'O'},
                          {'-', '-', 'O'}};
        
        assertEquals(true, OXProgram.checkVertical(table,'O',3));
    }
    
    //horizontal
    
    @Test
    public void test_o_win_horizontal1() {
        char table[][] = {{'O', 'O', 'O'}, 
                          {'-', '-', '-'},
                          {'-', '-', '-'}};
        
        assertEquals(true, OXProgram.checkHorizontal(table,'O',1));
    }
    
    @Test
    public void test_o_win_horizontal2() {
        char table[][] = {{'-', '-', '-'}, 
                          {'O', 'O', 'O'},
                          {'-', '-', '-'}};
        
        assertEquals(true, OXProgram.checkHorizontal(table,'O',2));
    }
    
    @Test
    public void test_o_win_horizontal3() {
        char table[][] = {{'-', '-', '-'}, 
                          {'-', '-', '-'},
                          {'O', 'O', 'O'}};
        
        assertEquals(true, OXProgram.checkHorizontal(table,'O',3));
    }
    
    //checkX
    
    @Test
    public void test_o_win_X1() {
        char table[][] = {{'O', '-', '-'}, 
                          {'-', 'O', '-'},
                          {'-', '-', 'O'}};
        
        assertEquals(true, OXProgram.checkX1(table,'O'));
    }
    
    @Test
    public void test_o_win_X2() {
        char table[][] = {{'-', '-', 'O'}, 
                          {'-', 'O', '-'},
                          {'O', '-', '-'}};
        
        assertEquals(true, OXProgram.checkX2(table,'O'));
    }
    
    //Player X
    
    //vertical
    
    @Test
    public void test_x_win_vertical1() {
        char table[][] = {{'X', '-', '-'}, 
                          {'X', '-', '-'},
                          {'X', '-', '-'}};
        
        assertEquals(true, OXProgram.checkVertical(table,'X',1));
    }
    
    @Test
    public void test_x_win_vertical2() {
        char table[][] = {{'-', 'X', '-'}, 
                          {'-', 'X', '-'},
                          {'-', 'X', '-'}};
        
        assertEquals(true, OXProgram.checkVertical(table,'X',2));
    }
    
    @Test
    public void test_x_win_vertical3() {
        char table[][] = {{'-', '-', 'X'}, 
                          {'-', '-', 'X'},
                          {'-', '-', 'X'}};
        
        assertEquals(true, OXProgram.checkVertical(table,'X',3));
    }
    
    //horizontal
    
    @Test
    public void test_x_win_horizontal1() {
        char table[][] = {{'X', 'X', 'X'}, 
                          {'-', '-', '-'},
                          {'-', '-', '-'}};
        
        assertEquals(true, OXProgram.checkHorizontal(table,'X',1));
    }
    
    @Test
    public void test_x_win_horizontal2() {
        char table[][] = {{'-', '-', '-'}, 
                          {'X', 'X', 'X'},
                          {'-', '-', '-'}};
        
        assertEquals(true, OXProgram.checkHorizontal(table,'X',2));
    }
    
    @Test
    public void test_x_win_horizontal3() {
        char table[][] = {{'-', '-', '-'}, 
                          {'-', '-', '-'},
                          {'X', 'X', 'X'}};
        
        assertEquals(true, OXProgram.checkHorizontal(table,'X',3));
    }
    
    //checkX
    
    @Test
    public void test_x_win_X1() {
        char table[][] = {{'X', '-', '-'}, 
                          {'-', 'X', '-'},
                          {'-', '-', 'X'}};
        
        assertEquals(true, OXProgram.checkX1(table,'X'));
    }
    
    @Test
    public void test_x_win_X2() {
        char table[][] = {{'-', '-', 'X'}, 
                          {'-', 'X', '-'},
                          {'X', '-', '-'}};
        
        assertEquals(true, OXProgram.checkX2(table,'X'));
    }
}
